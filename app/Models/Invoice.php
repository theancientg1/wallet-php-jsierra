<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model {

    protected $table = 'invoicing';

    protected $fillable = [
        'customer_email',
        'invoice_number',
        'ammount',
        'status',
        'owner'
    ];

}