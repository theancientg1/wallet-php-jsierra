<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Invoice;


class WalletController extends Controller
{
    public function deposit(Request $request){
        $amount = $request->get("amount");
        auth()->user()->deposit($amount);
        return view("dashboard");
    }

    public function withdraw(Request $request){
        $amount = $request->get("amount");
        auth()->user()->withdraw($amount);
        return view("dashboard");
    }

    public function register(Request $request) {
        $invoice = new Invoice();
        $invoice->fill($request->all());
        $invoice['owner'] = auth()->user()->id;
        $invoice->save();

        $invoices = Invoice::where('owner', auth()->user()->id)->get();
        return view("dashboard")->with('invoices', $invoices);
    }
}
