<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WalletController;
use App\Models\Invoice;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    $invoices = Invoice::where('owner', auth()->user()->id)->get();
    return view('dashboard')->with('invoices', $invoices);
})->middleware(['auth'])->name('dashboard');

Route::get('/deposit', function () {
    return view('deposit');
})->middleware(['auth'])->name('deposit');

Route::post('/perform/deposit', [App\Http\Controllers\WalletController::class, 'deposit'])->middleware(['auth'])->name('perform.deposit');

Route::get('/withdraw', function () {
    return view('withdraw');
})->middleware(['auth'])->name('withdraw');

Route::post('/perform/withdraw', [App\Http\Controllers\WalletController::class, 'withdraw'])->middleware(['auth'])->name('perform.withdraw');

Route::get('/create/invoice', function () {
    return view('create_invoice');
})->middleware(['auth'])->name('create.invoice');

Route::post('/register/invoice', [App\Http\Controllers\WalletController::class, 'register'])->middleware(['auth'])->name('register.invoice');


require __DIR__.'/auth.php';
