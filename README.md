# Wallet PHP JSierra V1.0

Aplicación Wallet hecha en Laravel y PHP para la  gestion de billeteras personales entre múltiples usuarios

# Instalación

1 - Hacer checkout del proyecto

Antes que nada, hay que clonar (o sincronizar) con los archivos del proyecto para asegurarse que cualquier cambio se mantenga.

2 - Instalar Composer

Composer es una herramienta de dependencias de PHP para las librerías de tu proyecto (Las librerías son manejadas de manera individual por proyecto, cada uno en su carpeta vendor respectiva). Debido a la naturaleza del proyecto, existe una alta cantidad de librerías que dependen de otras librerías y Composer hace su manejo mucho más fácil aparte de otras propiedades.

El comando de instalación es:

    install composer

3 - Editar el archivo .env

El siguiente paso es configurar las base de datos con las configuraciones necesarias (Dependiendo del ambiente que se esté trabajando, puede que se necesite crear la variable .env) con el siguiente bloque de código:

    DB_CONNECTION=mysql 


    DB_HOST=127.0.0.1 


    DB_PORT=3306 


    DB_DATABASE=databasename 


    DB_USERNAME=databaseuser 


    DB_PASSWORD=databasepassword

4 - Ejecutar las migraciones necesarias

A continuación se deben hacer las migraciones necesarias para la base de datos de la billetera, las cuales incluyen cualquier tabla o estructura de la misma. El comando es:

    php artisan migrate

5 - Levantar el servidor

Una vez ya configurada la base de datos y montado las migraciones, solo queda levantar el servidor como tal con el comando:

    php artisan serve

Ya con esto, la billetera está lista para ser manejada por el usuario

## Registro
Al entrar a la página, se le pedirá al usuario que se registre ingresando su nombre y su correo. Esto es para mantener un control sobre las facturas registradas y el monto actual entre todos los usuarios de la billetera aparte de ser organizado.

## Dashboard
Después de registrarse, el usuario podrá apreciar un conjunto de elementos. Primero sería una bienvenida a la billetera con el nombre que usó en el registro y después está la cantidad actual de su monto (Al ser un usuario completamente nuevo, el monto está en cero por default). Aparte de eso, hay una cantidad de opciones o acciones que el usuario puede hacer para manejar la billetera virtual.

## Acciones
- Depositar: Una acción simple donde el usuario ingresa un monto decimal que después se agregara al monto actual para que se refleje en la página principal de la billetera.

- Retirar: Acción opuesta de depositar. El usuario ingresa una número decimal que será restado del monto con la condición que no puede retirarse más de lo que está actualmente depositado. 

- Crear Factura: Está es la acción que nos permitirá tener un registro de facturas a pagar. Al usuario se le pedirá ingresar de nuevo su correo electrónico, su número de cédula,  el número de identificación de la factura y cual es el monto. Una vez ingresado, se podrá apreciar que la factura fue agregada al registro general de la billetera, donde aparte de mostrar la información previamente ingresada, también indica la fecha en cuando se realizó registro y cual es el estado actual (Pagada o pendiente).

- Pagar Factura: Esta acción final es la que nos permite pagar las facturas que son nuestras o las de otros usuarios que hayan usado la billetera. Primero se le pedirá indicar al usuario cual factura es la que desea pagar y ya siendo escogida, se pedirá indicar en qué forma de moneda se desea pagar la factura donde las opciones son dólares, Bs y tarjeta de débito o crédito.

Adicionalmente, debe permitir que el pago sea realizado por Zelle, Paypal o cualquier otra billetera electrónica externa. Habiendo indicado la forma de pago, ahora queda indicar que tanto de la factura se desea pagar en este momento y queda remarcar que un pago se considerará pendiente hasta que todo el monto completo sea pagado. 

En caso contrario, si  solo se paga una parte de este, se registrará como parcialmente pagado.
