<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    Bienvenido {{auth()->user()->name}}!
                    <br/>
                    Tu saldo actual es: {{auth()->user()->balance}}
                    <br/>
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('deposit') }}">
                        Depositar
                    </a>
                    <br/>
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('withdraw') }}">
                        Retirar
                    </a>
                    <br/>
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('create.invoice') }}">
                        Crear Factura
                    </a>
                    <br/>
                    <br/>
                    <table>
                        <th>Fecha</th>
                        <th>Numero</th>
                        <th>Cliente</th>
                        <th>Monto</th>
                        <th>Status</th>
                        @foreach($invoices as $invoice)
                            <tr>
                                <td>{{$invoice->created_at}}</td>
                                <td>{{$invoice->invoice_number}}</td>
                                <td>{{$invoice->customer_email}}</td>
                                <td>{{$invoice->ammount}}</td>
                                <td>{{$invoice->status}}</td>
                            </tr>
                        @endforeach
                    <table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
