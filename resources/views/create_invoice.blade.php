<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('register.invoice') }}">
            @csrf

            <!-- Input -->
            <div>
                <x-label for="customer_email" value="Correo" />

                <x-input id="customer_email" class="block mt-1 w-full" type="email" name="customer_email" required autofocus />
            </div>

            <div>
                <x-label for="invoice_number" value="Numero de Factura" />

                <x-input id="invoice_number" class="block mt-1 w-full" type="number" name="invoice_number"  required autofocus />
            </div>
            
            <div>
                <x-label for="ammount" value="Monto" />

                <x-input id="ammount" class="block mt-1 w-full" type="number" step="0.01" name="ammount" value="0.00" required autofocus />
            </div>

            <div class="flex items-center justify-end mt-4">

                <x-button class="ml-3">
                    Aceptar
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
